module.exports = {
	NODE_ENV: 'none',
	MODE_ENV: {
		local: {
			api: 'https://react-scb-demo.herokuapp.com/',
			show_log: true,
			hot: true,
			liveReload: true,
			host: "localhost",
			port: 1001,
			portServer: 5556,

		},
		dev: {
			api: 'https://react-scb-demo.herokuapp.com/'
		},
		sta: {
			api: 'https://react-scb-demo.herokuapp.com/'
		},
		prod: {
			api: 'https://react-scb-demo.herokuapp.com/'
		},
	}
};
