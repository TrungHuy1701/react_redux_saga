const dataSelectStatus = [
    { Status_id: 1, Status_name: 'Tất cả' },
    { Status_id: 2, Status_name: 'Đã hủy' },
    { Status_id: 3, Status_name: 'Đang chờ' },
    { Status_id: 4, Status_name: 'xác nhận' },
]
export default dataSelectStatus;