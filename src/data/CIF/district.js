const district = [
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 722,
        "name": "Bình Chánh",
        "code": "BC",
        "lon": 106.57576777844915,
        "lat": 10.733630505398786
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 723,
        "name": "Bình Tân",
        "code": "BT",
        "lon": 106.59748219906407,
        "lat": 10.764848030631404
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 739,
        "name": "Bình Thạnh",
        "code": "BT",
        "lon": 106.71577249255374,
        "lat": 10.81195810079888
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 755,
        "name": "Cần Giờ",
        "code": "CG",
        "lon": 106.8688294785764,
        "lat": 10.521892338722642
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 742,
        "name": "Củ Chi",
        "code": "CC",
        "lon": 106.5112075178779,
        "lat": 11.009231505431439
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 734,
        "name": "Gò Vấp",
        "code": "GV",
        "lon": 106.6672852385859,
        "lat": 10.838247272223343
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 733,
        "name": "Hóc Môn",
        "code": "HM",
        "lon": 106.58968415266395,
        "lat": 10.885212572709001
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 721,
        "name": "Nhà Bè",
        "code": "NB",
        "lon": 106.73017246387565,
        "lat": 10.654607835950932
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 732,
        "name": "Phú Nhuận",
        "code": "PN",
        "lon": 106.6781020964684,
        "lat": 10.800529736703698
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 730,
        "name": "Quận 1",
        "code": "Q1",
        "lon": 106.69727795124793,
        "lat": 10.776291138509789
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 729,
        "name": "Quận 10",
        "code": "Q10",
        "lon": 106.6682702191779,
        "lat": 10.772362637916292
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 725,
        "name": "Quận 11",
        "code": "Q11",
        "lon": 106.64763058840437,
        "lat": 10.764083475968704
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 735,
        "name": "Quận 12",
        "code": "Q12",
        "lon": 106.65718742220645,
        "lat": 10.864604897757495
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 738,
        "name": "Quận 2",
        "code": "Q2",
        "lon": 106.75572950419472,
        "lat": 10.77937744664325
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 731,
        "name": "Quận 3",
        "code": "Q3",
        "lon": 106.68276237067607,
        "lat": 10.78173377460698
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 737,
        "name": "Quận 4",
        "code": "Q4",
        "lon": 106.70563848573609,
        "lat": 10.75856992295204
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 726,
        "name": "Quận 5",
        "code": "Q5",
        "lon": 106.66944763417875,
        "lat": 10.754911290738892
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 724,
        "name": "Quận 6",
        "code": "Q6",
        "lon": 106.63529359561207,
        "lat": 10.74581417402403
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 736,
        "name": "Quận 7",
        "code": "Q7",
        "lon": 106.72930115339132,
        "lat": 10.736392920680318
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 720,
        "name": "Quận 8",
        "code": "Q8",
        "lon": 106.64240836942982,
        "lat": 10.72593850762322
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 741,
        "name": "Quận 9",
        "code": "Q9",
        "lon": 106.82250155678469,
        "lat": 10.823056753780369
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 728,
        "name": "Tân Bình",
        "code": "TB",
        "lon": 106.65224460153489,
        "lat": 10.808957728860944
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 727,
        "name": "Tân Phú",
        "code": "TP",
        "lon": 106.62682197079745,
        "lat": 10.792642852247187
    },
    {
        "province_id": 55,
        "province_avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "district_id": 740,
        "name": "Thủ Đức",
        "code": "TĐ",
        "lon": 106.74518356227864,
        "lat": 10.8556616054873
    }
]

export default district