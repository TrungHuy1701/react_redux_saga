const ward = [
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2274,
        "name": "An Khánh",
        "code": "AK",
        "lon": 106.71703691610011,
        "lat": 10.780334836883634
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2275,
        "name": "An Lợi Đông",
        "code": "ALĐ",
        "lon": 106.72623943586413,
        "lat": 10.76830121174885
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2276,
        "name": "An Phú",
        "code": "AP",
        "lon": 106.75695734997899,
        "lat": 10.798572720176669
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2277,
        "name": "Bình An",
        "code": "BA",
        "lon": 106.72956624510704,
        "lat": 10.791266709613124
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2278,
        "name": "Bình Khánh",
        "code": "BK",
        "lon": 106.73666721020543,
        "lat": 10.782552393958301
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2279,
        "name": "Bình Trưng Đông",
        "code": "BTĐ",
        "lon": 106.77799025171927,
        "lat": 10.78419600389291
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2280,
        "name": "Bình Trưng Tây",
        "code": "BTT",
        "lon": 106.7569319681413,
        "lat": 10.783796546577152
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2283,
        "name": "Cát Lái",
        "code": "CL",
        "lon": 106.78847900731147,
        "lat": 10.769742459908187
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2284,
        "name": "Thạnh Mỹ Lợi",
        "code": "TML",
        "lon": 106.76365701197174,
        "lat": 10.759615976811386
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2286,
        "name": "Thảo Điền",
        "code": "TĐ",
        "lon": 106.73278822235831,
        "lat": 10.80812779380266
    },
    {
        "district_id": 738,
        "province_id": 55,
        "ward_id": 2287,
        "name": "Thủ Thiêm",
        "code": "TT",
        "lon": 106.71541806215888,
        "lat": 10.771696327964046
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2189,
        "name": "An Phú Tây",
        "code": "APT",
        "lon": 106.60755940768867,
        "lat": 10.68755663915401
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2190,
        "name": "Bình Chánh",
        "code": "BC",
        "lon": 106.56694118727461,
        "lat": 10.663893933795407
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2191,
        "name": "Bình Hưng",
        "code": "BH",
        "lon": 106.67076734472195,
        "lat": 10.72111109806061
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2192,
        "name": "Bình Lợi",
        "code": "BL",
        "lon": 106.49590546148048,
        "lat": 10.750964438108502
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2193,
        "name": "Đa Phước",
        "code": "ĐP",
        "lon": 106.659255168388,
        "lat": 10.663277777417113
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2194,
        "name": "Hưng Long",
        "code": "HL",
        "lon": 106.62545730223061,
        "lat": 10.664082767119126
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2195,
        "name": "Lê Minh Xuân",
        "code": "LMX",
        "lon": 106.52677357029124,
        "lat": 10.755888281002543
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2196,
        "name": "Phạm Văn Hai",
        "code": "PVH",
        "lon": 106.52796956245943,
        "lat": 10.816297396412116
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2197,
        "name": "Phong Phú",
        "code": "PP",
        "lon": 106.65108686139362,
        "lat": 10.696316152107018
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2198,
        "name": "Quy Đức",
        "code": "QĐ",
        "lon": 106.6439251968755,
        "lat": 10.639797903231488
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2199,
        "name": "Tân Kiên",
        "code": "TK",
        "lon": 106.58447853422942,
        "lat": 10.714629179074505
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2200,
        "name": "Tân Nhựt",
        "code": "TN",
        "lon": 106.55091131413577,
        "lat": 10.711357789943284
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2201,
        "name": "Tân Quý Tây",
        "code": "TQT",
        "lon": 106.59509844468427,
        "lat": 10.667206436725918
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2188,
        "name": "Tân Túc",
        "code": "TT",
        "lon": 106.57449344815485,
        "lat": 10.685163831233087
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2202,
        "name": "Vĩnh Lộc A",
        "code": "VLA",
        "lon": 106.56118055076864,
        "lat": 10.823504338690919
    },
    {
        "district_id": 722,
        "province_id": 55,
        "ward_id": 2203,
        "name": "Vĩnh Lộc B",
        "code": "VLB",
        "lon": 106.56646757111704,
        "lat": 10.789357547013681
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 80,
        "name": "An Lạc",
        "code": "AL",
        "lon": 106.61187732741494,
        "lat": 10.725188587411216
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 88,
        "name": "An Lạc A",
        "code": "ALA",
        "lon": 106.6186029469046,
        "lat": 10.746427830161863
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 79,
        "name": "Bình Hưng Hòa",
        "code": "BHH",
        "lon": 106.60407932791792,
        "lat": 10.805626896320918
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 77,
        "name": "Bình Hưng Hòa A",
        "code": "BHHA",
        "lon": 106.60769160847981,
        "lat": 10.783845768194844
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 78,
        "name": "Bình Hưng Hòa B",
        "code": "BHHB",
        "lon": 106.59170725030455,
        "lat": 10.80547633080849
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 92,
        "name": "Bình Trị Đông",
        "code": "BTĐ",
        "lon": 106.61614641955653,
        "lat": 10.765981161978083
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 76,
        "name": "Bình Trị Đông A",
        "code": "BTĐA",
        "lon": 106.59662867661234,
        "lat": 10.76962672336968
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 74,
        "name": "Bình Trị Đông B",
        "code": "BTĐB",
        "lon": 106.6088503497902,
        "lat": 10.745271589297454
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 75,
        "name": "Tân Tạo",
        "code": "TT",
        "lon": 106.58880943266225,
        "lat": 10.759283251796921
    },
    {
        "district_id": 723,
        "province_id": 55,
        "ward_id": 73,
        "name": "Tân Tạo A",
        "code": "TTA",
        "lon": 106.58091111505539,
        "lat": 10.742232128641874
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 221,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.69714861304485,
        "lat": 10.79868747339039
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 242,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.69370736004072,
        "lat": 10.81574170879065
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 290,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.7005693383313,
        "lat": 10.81232251332294
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 293,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.70375476522334,
        "lat": 10.82665486609334
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 228,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.69677051928285,
        "lat": 10.805566503181295
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 284,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.70373018424745,
        "lat": 10.799239784183126
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 285,
        "name": "Phường 17",
        "code": "P17",
        "lon": 106.70634898721475,
        "lat": 10.796473666200024
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 282,
        "name": "Phường 19",
        "code": "P19",
        "lon": 106.70973022513311,
        "lat": 10.790672830415518
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 283,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.70077606224596,
        "lat": 10.800303056314387
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 287,
        "name": "Phường 21",
        "code": "P21",
        "lon": 106.71208752357765,
        "lat": 10.797142573729914
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 286,
        "name": "Phường 22",
        "code": "P22",
        "lon": 106.71899518709421,
        "lat": 10.792860197428663
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 288,
        "name": "Phường 24",
        "code": "P24",
        "lon": 106.70494067312514,
        "lat": 10.80554162168876
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 289,
        "name": "Phường 25",
        "code": "P25",
        "lon": 106.718100565502,
        "lat": 10.805056271227496
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 291,
        "name": "Phường 26",
        "code": "P26",
        "lon": 106.70988472522934,
        "lat": 10.813466700793017
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 292,
        "name": "Phường 27",
        "code": "P27",
        "lon": 106.71983326239035,
        "lat": 10.81635151886403
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 294,
        "name": "Phường 28",
        "code": "P28",
        "lon": 106.74092286811926,
        "lat": 10.821861044331369
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 220,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.69369869036983,
        "lat": 10.79898840245263
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 239,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.68711869058467,
        "lat": 10.81183677237355
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 225,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.6876538533841,
        "lat": 10.806032543451193
    },
    {
        "district_id": 739,
        "province_id": 55,
        "ward_id": 227,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.69183236398385,
        "lat": 10.807154657405185
    },
    {
        "district_id": 755,
        "province_id": 55,
        "ward_id": 2205,
        "name": "An Thới Đông",
        "code": "ATĐ",
        "lon": 106.81115655956278,
        "lat": 10.550843374998685
    },
    {
        "district_id": 755,
        "province_id": 55,
        "ward_id": 2206,
        "name": "Bình Khánh",
        "code": "BK",
        "lon": 106.7891331780007,
        "lat": 10.637923159024881
    },
    {
        "district_id": 755,
        "province_id": 55,
        "ward_id": 2204,
        "name": "Cần Thạnh",
        "code": "CT",
        "lon": 106.94428120028068,
        "lat": 10.416863161743779
    },
    {
        "district_id": 755,
        "province_id": 55,
        "ward_id": 2207,
        "name": "Long Hòa",
        "code": "LH",
        "lon": 106.90312612826807,
        "lat": 10.454913547150586
    },
    {
        "district_id": 755,
        "province_id": 55,
        "ward_id": 2208,
        "name": "Lý Nhơn",
        "code": "LN",
        "lon": 106.79969812092561,
        "lat": 10.473725511186332
    },
    {
        "district_id": 755,
        "province_id": 55,
        "ward_id": 2209,
        "name": "Tam Thôn Hiệp",
        "code": "TTH",
        "lon": 106.88303463922578,
        "lat": 10.580721636637882
    },
    {
        "district_id": 755,
        "province_id": 55,
        "ward_id": 2210,
        "name": "Thạnh An",
        "code": "TA",
        "lon": 106.97361431414868,
        "lat": 10.547331100094343
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2213,
        "name": "An Nhơn Tây",
        "code": "ANT",
        "lon": 106.48479968322025,
        "lat": 11.074928563876666
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2214,
        "name": "An Phú",
        "code": "AP",
        "lon": 106.49901339313159,
        "lat": 11.112808967241708
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2216,
        "name": "Bình Mỹ",
        "code": "BM",
        "lon": 106.6331910981942,
        "lat": 10.942685729486987
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2212,
        "name": "Củ Chi",
        "code": "CC",
        "lon": 106.49366030940845,
        "lat": 10.973880398835009
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2217,
        "name": "Hòa Phú",
        "code": "HP",
        "lon": 106.61104687902264,
        "lat": 10.978627564949777
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2218,
        "name": "Nhuận Đức",
        "code": "NĐ",
        "lon": 106.49708717935799,
        "lat": 11.036649928036065
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2222,
        "name": "Phạm Văn Cội",
        "code": "PVC",
        "lon": 106.52292033469622,
        "lat": 11.03715669739779
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2223,
        "name": "Phú Hòa Đông",
        "code": "PHĐ",
        "lon": 106.55543945210633,
        "lat": 11.021158602736882
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2225,
        "name": "Phú Mỹ Hưng",
        "code": "PMH",
        "lon": 106.46181597040827,
        "lat": 11.121369620094484
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2227,
        "name": "Phước Hiệp",
        "code": "PH",
        "lon": 106.44817097906065,
        "lat": 10.983178899809495
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2229,
        "name": "Phước Thạnh",
        "code": "PT",
        "lon": 106.42746950784665,
        "lat": 11.015047605445655
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2231,
        "name": "Phước Vĩnh An",
        "code": "PVA",
        "lon": 106.52194707733544,
        "lat": 10.981862749322598
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2232,
        "name": "Tân An Hội",
        "code": "TAH",
        "lon": 106.47814785325194,
        "lat": 10.963335240662065
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2234,
        "name": "Tân Phú Trung",
        "code": "TPT",
        "lon": 106.55128536601995,
        "lat": 10.94061211508886
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2235,
        "name": "Tân Thạnh Đông",
        "code": "TTĐ",
        "lon": 106.59404257199158,
        "lat": 10.953554400300012
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2237,
        "name": "Tân Thạnh Tây",
        "code": "TTT",
        "lon": 106.55815245745588,
        "lat": 10.987129597858099
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2238,
        "name": "Tân Thông Hội",
        "code": "TTH",
        "lon": 106.51022993809251,
        "lat": 10.943685381922931
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2240,
        "name": "Thái Mỹ",
        "code": "TM",
        "lon": 106.39759769877601,
        "lat": 10.985697637668299
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2242,
        "name": "Trung An",
        "code": "TA",
        "lon": 106.59114462407373,
        "lat": 11.008690548291321
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2245,
        "name": "Trung Lập Hạ",
        "code": "TLH",
        "lon": 106.46624375760605,
        "lat": 11.01894079955911
    },
    {
        "district_id": 742,
        "province_id": 55,
        "ward_id": 2247,
        "name": "Trung Lập Thượng",
        "code": "TLT",
        "lon": 106.43864759023963,
        "lat": 11.06352521209955
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 243,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.68794383379836,
        "lat": 10.816967467160435
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 238,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.67070605984527,
        "lat": 10.832523637242636
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 246,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.65741100344357,
        "lat": 10.84025599712451
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 234,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.64354952152519,
        "lat": 10.843630257650485
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 255,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.65800852627001,
        "lat": 10.85551768043894
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 236,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.64356510864921,
        "lat": 10.847932509124309
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 249,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.6707154092824,
        "lat": 10.852800552943785
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 248,
        "name": "Phường 16",
        "code": "P16",
        "lon": 106.66485686243942,
        "lat": 10.84584798832692
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 247,
        "name": "Phường 17",
        "code": "P17",
        "lon": 106.68006354816247,
        "lat": 10.842016352887008
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 240,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.67850033565588,
        "lat": 10.81815585884196
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 241,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.68489466272551,
        "lat": 10.821186117045462
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 245,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.69195945534867,
        "lat": 10.828112953874879
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 250,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.68311602088274,
        "lat": 10.842809138698321
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 244,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.68333390073678,
        "lat": 10.828900740570171
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 235,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.65034343841509,
        "lat": 10.840580436855591
    },
    {
        "district_id": 734,
        "province_id": 55,
        "ward_id": 237,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.65217752232122,
        "lat": 10.846475109634914
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2252,
        "name": "Bà Điểm",
        "code": "BĐ",
        "lon": 106.5987410034672,
        "lat": 10.841264371305968
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2254,
        "name": "Đông Thạnh",
        "code": "ĐT",
        "lon": 106.64079143485814,
        "lat": 10.903340574001316
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2251,
        "name": "Hóc Môn",
        "code": "HM",
        "lon": 106.59252812316264,
        "lat": 10.886769053464144
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2256,
        "name": "Nhị Bình",
        "code": "NB",
        "lon": 106.67228306617747,
        "lat": 10.912040537704875
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2258,
        "name": "Tân Hiệp",
        "code": "TH",
        "lon": 106.5885361595311,
        "lat": 10.910356666253413
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2260,
        "name": "Tân Thới Nhì",
        "code": "TTN",
        "lon": 106.54992605798368,
        "lat": 10.90354091560936
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2261,
        "name": "Tân Xuân",
        "code": "TX",
        "lon": 106.60099420248524,
        "lat": 10.87553089890573
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2262,
        "name": "Thới Tam Thôn",
        "code": "TTT",
        "lon": 106.61249348615037,
        "lat": 10.891870771922674
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2263,
        "name": "Trung Chánh",
        "code": "TC",
        "lon": 106.608569457277,
        "lat": 10.8660165806468
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2264,
        "name": "Xuân Thới Đông",
        "code": "XTĐ",
        "lon": 106.59177357499037,
        "lat": 10.866265947846639
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2265,
        "name": "Xuân Thới Sơn",
        "code": "XTS",
        "lon": 106.55644547959635,
        "lat": 10.879208475169577
    },
    {
        "district_id": 733,
        "province_id": 55,
        "ward_id": 2266,
        "name": "Xuân Thới Thượng",
        "code": "XTT",
        "lon": 106.56148300930951,
        "lat": 10.855121778271553
    },
    {
        "district_id": 721,
        "province_id": 55,
        "ward_id": 2268,
        "name": "Hiệp Phước",
        "code": "HP",
        "lon": 106.7517145895672,
        "lat": 10.6085081062583
    },
    {
        "district_id": 721,
        "province_id": 55,
        "ward_id": 2269,
        "name": "Long Thới",
        "code": "LT",
        "lon": 106.7249786893255,
        "lat": 10.65387316685936
    },
    {
        "district_id": 721,
        "province_id": 55,
        "ward_id": 2267,
        "name": "Nhà Bè",
        "code": "NB",
        "lon": 106.74185221514465,
        "lat": 10.693669897716926
    },
    {
        "district_id": 721,
        "province_id": 55,
        "ward_id": 2270,
        "name": "Nhơn Đức",
        "code": "NĐ",
        "lon": 106.69937318416136,
        "lat": 10.67255532617544
    },
    {
        "district_id": 721,
        "province_id": 55,
        "ward_id": 2272,
        "name": "Phước Kiểng",
        "code": "PK",
        "lon": 106.70920593567863,
        "lat": 10.704274146470711
    },
    {
        "district_id": 721,
        "province_id": 55,
        "ward_id": 2273,
        "name": "Phước Lộc",
        "code": "PL",
        "lon": 106.6835707678538,
        "lat": 10.698579801500602
    },
    {
        "district_id": 721,
        "province_id": 55,
        "ward_id": 2271,
        "name": "Phú Xuân",
        "code": "PX",
        "lon": 106.74984297083512,
        "lat": 10.676560137022273
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 217,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.68223923805353,
        "lat": 10.798863190089554
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 209,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.6706737081479,
        "lat": 10.795509065444806
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 207,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.67428073944173,
        "lat": 10.793091765024846
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 215,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.67814541054913,
        "lat": 10.792390800405572
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 205,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.66969901129382,
        "lat": 10.789998299261617
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 206,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.66807618593906,
        "lat": 10.792019847286527
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 2314,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.67911484220586,
        "lat": 10.796626123881994
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 2315,
        "name": "Phường 17",
        "code": "P17",
        "lon": 106.68224597325937,
        "lat": 10.79363988088595
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 218,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.68613264265785,
        "lat": 10.797370034084045
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 222,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.68105815422325,
        "lat": 10.802449181149052
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 223,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.67978367081979,
        "lat": 10.807456297082172
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 224,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.68315313816184,
        "lat": 10.807125449962651
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 226,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.68825721685108,
        "lat": 10.800789126980664
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 210,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.67362209501235,
        "lat": 10.798167389950816
    },
    {
        "district_id": 732,
        "province_id": 55,
        "ward_id": 213,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.673902809432,
        "lat": 10.805253451056332
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 280,
        "name": "Bến Nghé",
        "code": "BN",
        "lon": 106.70419888710137,
        "lat": 10.780724003154672
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 197,
        "name": "Bến Thành",
        "code": "BT",
        "lon": 106.69391409246985,
        "lat": 10.773428041664465
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 157,
        "name": "Cầu Kho",
        "code": "CK",
        "lon": 106.68832161883684,
        "lat": 10.757519563556556
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 161,
        "name": "Cầu Ông Lãnh",
        "code": "CL",
        "lon": 106.69652460125194,
        "lat": 10.765387139203215
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 160,
        "name": "Cô Giang",
        "code": "CG",
        "lon": 106.69325620024352,
        "lat": 10.76218092605739
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 281,
        "name": "Đa Kao",
        "code": "ĐK",
        "lon": 106.69818930509308,
        "lat": 10.78898306986397
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 156,
        "name": "Nguyễn Cư Trinh",
        "code": "NCT",
        "lon": 106.68573441281949,
        "lat": 10.763022042934868
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 279,
        "name": "Nguyễn Thái Bình",
        "code": "NTB",
        "lon": 106.70092186445927,
        "lat": 10.768763397535453
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 196,
        "name": "Phạm Ngũ Lão",
        "code": "PNL",
        "lon": 106.69040001374454,
        "lat": 10.767999073155222
    },
    {
        "district_id": 730,
        "province_id": 55,
        "ward_id": 219,
        "name": "Tân Định",
        "code": "TĐ",
        "lon": 106.69016577700086,
        "lat": 10.792586446610258
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 155,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.67717504715633,
        "lat": 10.765188037880252
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 183,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.67223155440593,
        "lat": 10.769257310466148
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 194,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.67785810035059,
        "lat": 10.773412420561147
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 184,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.67176852905605,
        "lat": 10.774604744441238
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 187,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.67002255423881,
        "lat": 10.779156888715933
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 181,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.66079425599192,
        "lat": 10.77260215144283
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 185,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.66494063952943,
        "lat": 10.781977449404595
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 144,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.67387937728441,
        "lat": 10.763922569330404
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 141,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.67113599881897,
        "lat": 10.761954052139101
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 142,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.66914492717345,
        "lat": 10.763625675699767
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 140,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.66649887074034,
        "lat": 10.76157448109229
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 136,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.66316727575106,
        "lat": 10.762292803528975
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 135,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.66072496294946,
        "lat": 10.761411852448228
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 137,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.66567363257505,
        "lat": 10.765273584342886
    },
    {
        "district_id": 729,
        "province_id": 55,
        "ward_id": 143,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.67027055296745,
        "lat": 10.76666345420844
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 103,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.63880713123856,
        "lat": 10.756949504387878
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 105,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.64303928802202,
        "lat": 10.762795897255202
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 112,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.6502128041999,
        "lat": 10.766271378745824
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 109,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.65279800423716,
        "lat": 10.761342662884433
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 113,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.65346168039356,
        "lat": 10.764094602661656
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 167,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.64804665542499,
        "lat": 10.769123248970498
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 180,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.65555636866455,
        "lat": 10.769739648487795
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 107,
        "name": "Phường 16",
        "code": "P16",
        "lon": 106.64807926635856,
        "lat": 10.755705374736646
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 106,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.64382856511449,
        "lat": 10.756959759314265
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 104,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.63810512565531,
        "lat": 10.763576702351987
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 132,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.6550772832187,
        "lat": 10.758246390659258
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 165,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.64340209519493,
        "lat": 10.77069988698285
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 108,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.65480411724266,
        "lat": 10.760123061154793
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 133,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.65875014896416,
        "lat": 10.76093661896177
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 111,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.64833399761578,
        "lat": 10.761390530093538
    },
    {
        "district_id": 725,
        "province_id": 55,
        "ward_id": 110,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.64513289464601,
        "lat": 10.761765846428043
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 295,
        "name": "An Phú Đông",
        "code": "PAPĐ",
        "lon": 106.69779304428025,
        "lat": 10.854322218514923
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 233,
        "name": "Đông Hưng Thuận",
        "code": "PĐHT",
        "lon": 106.62783194067582,
        "lat": 10.841644732506786
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 254,
        "name": "Hiệp Thành",
        "code": "PHT",
        "lon": 106.63875387088642,
        "lat": 10.88099644151107
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 252,
        "name": "Tân Chánh Hiệp",
        "code": "PTCH",
        "lon": 106.62587540096185,
        "lat": 10.864233177015821
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 232,
        "name": "Tân Hưng Thuận",
        "code": "PTHT",
        "lon": 106.6228684138058,
        "lat": 10.838854654874218
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 253,
        "name": "Tân Thới Hiệp",
        "code": "PTTH",
        "lon": 106.64077103455739,
        "lat": 10.862147331516429
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 230,
        "name": "Tân Thới Nhất",
        "code": "PTTN",
        "lon": 106.61480371708791,
        "lat": 10.828828737501707
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 258,
        "name": "Thạnh Lộc",
        "code": "PTL",
        "lon": 106.68499072587738,
        "lat": 10.875456358020749
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 257,
        "name": "Thạnh Xuân",
        "code": "PTX",
        "lon": 106.67156635450412,
        "lat": 10.881472553984446
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 256,
        "name": "Thới An",
        "code": "PTA",
        "lon": 106.65493877404714,
        "lat": 10.875094696397689
    },
    {
        "district_id": 735,
        "province_id": 55,
        "ward_id": 251,
        "name": "Trung Mỹ Tây",
        "code": "PTMT",
        "lon": 106.61597380607849,
        "lat": 10.853661069684689
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 182,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.67694537888556,
        "lat": 10.768342067123115
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 188,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.67532356187517,
        "lat": 10.781697953735979
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 189,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.66952213614144,
        "lat": 10.786565298806766
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 190,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.67380549090747,
        "lat": 10.7881826826914
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 199,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.67799062747589,
        "lat": 10.786176877013661
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 214,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.6790474246631,
        "lat": 10.788916165118572
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 191,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.68131241420468,
        "lat": 10.767588121340822
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 192,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.67908388531174,
        "lat": 10.770433751023146
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 195,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.68243234282221,
        "lat": 10.773948469344964
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 193,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.68534319341661,
        "lat": 10.77179688881617
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 201,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.6912665372549,
        "lat": 10.78046076722332
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 200,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.6853655357073,
        "lat": 10.782923337250375
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 216,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.68650381061175,
        "lat": 10.789219224948258
    },
    {
        "district_id": 731,
        "province_id": 55,
        "ward_id": 198,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.67934413042093,
        "lat": 10.782846244731225
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 150,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.69091081918418,
        "lat": 10.754357647988686
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 2290,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.70514936317366,
        "lat": 10.760704537339835
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 273,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.70532609241758,
        "lat": 10.766221321721458
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 272,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.70971452397123,
        "lat": 10.76245652306685
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 271,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.7083818139835,
        "lat": 10.758271634082512
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 269,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.70750096750147,
        "lat": 10.754423812567492
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 274,
        "name": "Phường 16",
        "code": "P16",
        "lon": 106.71222460009943,
        "lat": 10.755337967016217
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 275,
        "name": "Phường 18",
        "code": "P18",
        "lon": 106.71671052132184,
        "lat": 10.757599313238616
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 158,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.69481145816941,
        "lat": 10.7567381512933
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 151,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.69879321591955,
        "lat": 10.754604054924123
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 2291,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.7029763521303,
        "lat": 10.75582587327213
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 159,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.6973173960825,
        "lat": 10.758971655105967
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 270,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.69942700047507,
        "lat": 10.76134582622742
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 2292,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.70308854878787,
        "lat": 10.760114140598818
    },
    {
        "district_id": 737,
        "province_id": 55,
        "ward_id": 2294,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.70191731454129,
        "lat": 10.763581862778064
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 148,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.68174727258773,
        "lat": 10.753523301126716
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 126,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.6626161868001,
        "lat": 10.75094244447326
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 127,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.66250295140402,
        "lat": 10.753815085879799
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 134,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.66067123200054,
        "lat": 10.756735942651806
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 123,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.65625932172512,
        "lat": 10.749208102014912
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 101,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.65472597353816,
        "lat": 10.75259631496449
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 102,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.65351557709631,
        "lat": 10.755426075755837
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 152,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.68012568919987,
        "lat": 10.756665115014057
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 153,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.67915603085102,
        "lat": 10.75878004993504
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 154,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.6785075095322,
        "lat": 10.76158062691202
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 131,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.67460422661674,
        "lat": 10.751482138190559
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 129,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.6699594249987,
        "lat": 10.751046495461383
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 130,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.67163312609202,
        "lat": 10.754179858415055
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 138,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.67087001050828,
        "lat": 10.756103244692778
    },
    {
        "district_id": 726,
        "province_id": 55,
        "ward_id": 139,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.67032353107092,
        "lat": 10.758736263379829
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 98,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.65090410938141,
        "lat": 10.746458822922966
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 81,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.62780045633964,
        "lat": 10.737240320762677
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 89,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.6320468127027,
        "lat": 10.745015862785339
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 90,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.63261639581344,
        "lat": 10.749939622554141
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 91,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.6274229499404,
        "lat": 10.752789877548777
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 93,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.63069296261963,
        "lat": 10.757735397880515
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 100,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.65024131688754,
        "lat": 10.7501946784315
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 87,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.64586896654139,
        "lat": 10.742900150215071
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 97,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.64499576647508,
        "lat": 10.745987922495775
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 95,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.64312982456126,
        "lat": 10.748519437202495
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 99,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.64549801086947,
        "lat": 10.751982899980618
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 83,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.63814353719614,
        "lat": 10.738622974534257
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 84,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.6389858386328,
        "lat": 10.742949994765501
    },
    {
        "district_id": 724,
        "province_id": 55,
        "ward_id": 96,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.63994023074588,
        "lat": 10.752482056937355
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 267,
        "name": "Bình Thuận",
        "code": "BT",
        "lon": 106.72315923816639,
        "lat": 10.742706267035018
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 72,
        "name": "Phú Mỹ",
        "code": "PM",
        "lon": 106.73678789591195,
        "lat": 10.707925806661077
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 278,
        "name": "Phú Thuận",
        "code": "PT",
        "lon": 106.74639431080995,
        "lat": 10.730578238522504
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 121,
        "name": "Tân Hưng",
        "code": "TH",
        "lon": 106.69792886897555,
        "lat": 10.745432696232369
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 268,
        "name": "Tân Kiểng",
        "code": "TK",
        "lon": 106.71021018312007,
        "lat": 10.748668243986911
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 260,
        "name": "Tân Phong",
        "code": "TP",
        "lon": 106.70542788800225,
        "lat": 10.729564953320713
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 266,
        "name": "Tân Phú",
        "code": "TP",
        "lon": 106.72529475499151,
        "lat": 10.726953889827678
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 265,
        "name": "Tân Quy",
        "code": "TQ",
        "lon": 106.70983104571174,
        "lat": 10.741832248562266
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 277,
        "name": "Tân Thuận Đông",
        "code": "TTĐ",
        "lon": 106.7387915542992,
        "lat": 10.75863750651219
    },
    {
        "district_id": 736,
        "province_id": 55,
        "ward_id": 276,
        "name": "Tân Thuận Tây",
        "code": "TTT",
        "lon": 106.72161429860653,
        "lat": 10.750967759129463
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 149,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.68984764614353,
        "lat": 10.74562352506053
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 124,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.66475095267964,
        "lat": 10.746000508115793
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 125,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.66339103927477,
        "lat": 10.748022456457482
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 115,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.65647289243208,
        "lat": 10.743311368558853
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 122,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.65543947180211,
        "lat": 10.74551880137997
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 86,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.64713684250724,
        "lat": 10.739332558344492
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 82,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.63347303602481,
        "lat": 10.725948860137546
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 68,
        "name": "Phường 16",
        "code": "P16",
        "lon": 106.6199326147037,
        "lat": 10.719160958767016
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 147,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.68619363607266,
        "lat": 10.746119028390007
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 145,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.68230011158064,
        "lat": 10.74471312761343
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 116,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.67572682902234,
        "lat": 10.740569220302561
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 114,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.66281470695118,
        "lat": 10.738254369912088
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 85,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.64890764004403,
        "lat": 10.7340292692542
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 67,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.62052747247435,
        "lat": 10.706663470848838
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 146,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.67894898840026,
        "lat": 10.749740669404508
    },
    {
        "district_id": 720,
        "province_id": 55,
        "ward_id": 128,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.6705489939935,
        "lat": 10.746383082766586
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2298,
        "name": "Hiệp Phú",
        "code": "HP",
        "lon": 106.77930742747719,
        "lat": 10.847646796004087
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2299,
        "name": "Long Bình",
        "code": "LB",
        "lon": 106.83617046694661,
        "lat": 10.867689205329729
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2303,
        "name": "Long Phước",
        "code": "LP",
        "lon": 106.85790132741505,
        "lat": 10.804259559302793
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2304,
        "name": "Long Thạnh Mỹ",
        "code": "LTM",
        "lon": 106.82051181786804,
        "lat": 10.84545815197306
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2305,
        "name": "Long Trường",
        "code": "LT",
        "lon": 106.82317024998974,
        "lat": 10.79379689336662
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2306,
        "name": "Phú Hữu",
        "code": "PH",
        "lon": 106.79976555223037,
        "lat": 10.793102330021048
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2307,
        "name": "Phước Bình",
        "code": "PB",
        "lon": 106.7715940611866,
        "lat": 10.81355892150328
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2308,
        "name": "Phước Long A",
        "code": "PLA",
        "lon": 106.76413770593251,
        "lat": 10.820936016119255
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2309,
        "name": "Phước Long B",
        "code": "PLB",
        "lon": 106.77933565366952,
        "lat": 10.817592341220536
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2311,
        "name": "Tăng Nhơn Phú A",
        "code": "TNPA",
        "lon": 106.79424902406454,
        "lat": 10.84292641738019
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2312,
        "name": "Tăng Nhơn Phú B",
        "code": "TNPB",
        "lon": 106.78850480443838,
        "lat": 10.828975799939935
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2310,
        "name": "Tân Phú",
        "code": "TP",
        "lon": 106.80338581392942,
        "lat": 10.859275850835738
    },
    {
        "district_id": 741,
        "province_id": 55,
        "ward_id": 2313,
        "name": "Trường Thạnh",
        "code": "TT",
        "lon": 106.82945900320806,
        "lat": 10.812012376247072
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 208,
        "name": "Phường 1",
        "code": "P1",
        "lon": 106.66619836264015,
        "lat": 10.79616097404656
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 170,
        "name": "Phường 10",
        "code": "P10",
        "lon": 106.64628870567309,
        "lat": 10.78228153818365
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 177,
        "name": "Phường 11",
        "code": "P11",
        "lon": 106.6477455275851,
        "lat": 10.789455967682942
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 179,
        "name": "Phường 12",
        "code": "P12",
        "lon": 106.6492983938041,
        "lat": 10.800252944260146
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 178,
        "name": "Phường 13",
        "code": "P13",
        "lon": 106.64229102727417,
        "lat": 10.803921711925165
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 176,
        "name": "Phường 14",
        "code": "P14",
        "lon": 106.64052837264366,
        "lat": 10.794704571376284
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 231,
        "name": "Phường 15",
        "code": "P15",
        "lon": 106.64916458212912,
        "lat": 10.822234316296482
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 212,
        "name": "Phường 2",
        "code": "P2",
        "lon": 106.66613518483543,
        "lat": 10.809354873860483
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 204,
        "name": "Phường 3",
        "code": "P3",
        "lon": 106.66205691859976,
        "lat": 10.793677862791428
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 211,
        "name": "Phường 4",
        "code": "P4",
        "lon": 106.65757571896096,
        "lat": 10.80438298426107
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 203,
        "name": "Phường 5",
        "code": "P5",
        "lon": 106.6618614242941,
        "lat": 10.790221824806242
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 186,
        "name": "Phường 6",
        "code": "P6",
        "lon": 106.65857710454411,
        "lat": 10.784066977523405
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 202,
        "name": "Phường 7",
        "code": "P7",
        "lon": 106.65558850180595,
        "lat": 10.788657349485872
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 171,
        "name": "Phường 8",
        "code": "P8",
        "lon": 106.6521234689305,
        "lat": 10.783272546144266
    },
    {
        "district_id": 728,
        "province_id": 55,
        "ward_id": 168,
        "name": "Phường 9",
        "code": "P9",
        "lon": 106.65145156809602,
        "lat": 10.775576497954694
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 162,
        "name": "Hiệp Tân",
        "code": "HT",
        "lon": 106.62693012810632,
        "lat": 10.771789391328152
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 169,
        "name": "Hòa Thạnh",
        "code": "HT",
        "lon": 106.63640157076013,
        "lat": 10.778684859859219
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 163,
        "name": "Phú Thạnh",
        "code": "PT",
        "lon": 106.62614869600442,
        "lat": 10.779565421308337
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 164,
        "name": "Phú Thọ Hòa",
        "code": "PTH",
        "lon": 106.62744039414325,
        "lat": 10.785369205706159
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 166,
        "name": "Phú Trung",
        "code": "PT",
        "lon": 106.64059502711319,
        "lat": 10.7760893904081
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 174,
        "name": "Sơn Kỳ",
        "code": "SK",
        "lon": 106.61672304902191,
        "lat": 10.804502674487711
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 172,
        "name": "Tân Quý",
        "code": "TQ",
        "lon": 106.621478805795,
        "lat": 10.794164490765617
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 173,
        "name": "Tân Sơn Nhì",
        "code": "TSN",
        "lon": 106.63130740884003,
        "lat": 10.79901132140681
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 175,
        "name": "Tân Thành",
        "code": "TT",
        "lon": 106.63411872761965,
        "lat": 10.790900887408032
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 94,
        "name": "Tân Thới Hòa",
        "code": "TTH",
        "lon": 106.63014870120409,
        "lat": 10.764087781871142
    },
    {
        "district_id": 727,
        "province_id": 55,
        "ward_id": 229,
        "name": "Tây Thạnh",
        "code": "TT",
        "lon": 106.62267169506995,
        "lat": 10.813294098983945
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 302,
        "name": "Bình Chiểu",
        "code": "BC",
        "lon": 106.72888895417404,
        "lat": 10.881493593270656
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 300,
        "name": "Bình Thọ",
        "code": "BT",
        "lon": 106.76586116335798,
        "lat": 10.846125532482018
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 297,
        "name": "Hiệp Bình Chánh",
        "code": "HBC",
        "lon": 106.72527665814154,
        "lat": 10.835219200299955
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 296,
        "name": "Hiệp Bình Phước",
        "code": "HBP",
        "lon": 106.71546530260817,
        "lat": 10.850691387844428
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 304,
        "name": "Linh Chiểu",
        "code": "LC",
        "lon": 106.76314078128812,
        "lat": 10.85387847459603
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 299,
        "name": "Linh Đông",
        "code": "LĐ",
        "lon": 106.74512952072611,
        "lat": 10.847185735297652
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 303,
        "name": "Linh Tây",
        "code": "LT",
        "lon": 106.75508528857503,
        "lat": 10.859491937688668
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 305,
        "name": "Linh Trung",
        "code": "LT",
        "lon": 106.78048266983785,
        "lat": 10.86439933719051
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 2316,
        "name": "Linh Xuân",
        "code": "LX",
        "lon": 106.77107151173541,
        "lat": 10.87988368847041
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 301,
        "name": "Tam Bình",
        "code": "TB",
        "lon": 106.73441460470762,
        "lat": 10.867864435259404
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 2317,
        "name": "Tam Phú",
        "code": "TP",
        "lon": 106.74020585328356,
        "lat": 10.858779326534334
    },
    {
        "district_id": 740,
        "province_id": 55,
        "ward_id": 298,
        "name": "Trường Thọ",
        "code": "TT",
        "lon": 106.75596182417475,
        "lat": 10.833802169778885
    }
]

export default ward