const city = [
    {
        "province_id": 55,
        "name": "Hồ Chí Minh",
        "code": "HCM",
        "avatar": "https://cloudapi.minerva.vn/cdn/minerva/province/TP.Ho-Chi-Minh.png",
        "lon": 106.72714590449307,
        "lat": 10.700268054894254
    },
]

export default city