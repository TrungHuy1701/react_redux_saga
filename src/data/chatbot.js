let chatbot = [
    {
        id: 1,
        name: "Chuyển tiền",
        age: "22",
        checked: false,
        children: [
            {
                question: [
                    { name: "Hi bạn, hỏi về chuyển tiền 1", id: 1, readOnly: true },
                    { name: "Hi bạn, hỏi về chuyển tiền 2", id: 2, readOnly: true },
                    { name: "Hi bạn, hỏi về chuyển tiền 3", id: 3, readOnly: true }
                ],
                answer: { name: "Tư vấn SCB xin chào Quý khách! Vui lòng chọn nội dung cần hỗ trợ: Chuyển tiền vào TK chứng khoán Chuyển tiền ra TK ngân hàng Chuyển tiền nhưng tiền chưa đến", anwser_id: 1, readOnly: true }
            },
            {
                question: [
                    { name: "Hi bạn, hỏi về chuyển tiền 4", id: 4, readOnly: true },
                    { name: "Hi bạn, hỏi về chuyển tiền 5", id: 5, readOnly: true },
                    { name: "Hi bạn, hỏi về chuyển tiền 6", id: 6, readOnly: true }
                ],
                answer: { name: "Tư vấn SCB xin chào Quý khách! Vui lòng chọn nội dung cần hỗ trợ: Chuyển tiền vào TK chứng khoán Chuyển tiền ra TK ngân hàng Chuyển tiền nhưng tiền chưa đến", anwser_id: 2, readOnly: true }
            }
        ]
    },
    {
        id: 2,
        name: "Soft OTP",
        age: "22",
        checked: false,
        children: [{
            question: [
                { name: "Hi bạn, hỏi về Soft OTP", id: 7, readOnly: true },
                { name: "Hi bạn, hỏi về Soft OTP", id: 8, readOnly: true },
                { name: "Hi bạn, hỏi về Soft OTP 3", id: 9, readOnly: true }
            ],
            answer: { name: "Tư vấn SCB xin chào Quý khách! Vui lòng chọn nội dung cần hỗ trợ: Chuyển tiền vào TK chứng khoán Chuyển tiền ra TK ngân hàngChuyển tiền nhưng tiền chưa đến", anwser_id: 3, readOnly: true }
        }]
    }
]

export default chatbot;