const optionsCol = {
    series: [{
        name: 'Tham gia',
        data: [76, 85, 100, 98, 87, 105, 91, 100, 94]
    }, {
        name: 'Rời khỏi',
        data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
    }],
    chart: {
        type: 'bar',
        height: "500",
        toolbar: {
            show: false,
        },
        dropShadow: {
            enabled: false,
            enabledOnSeries: undefined,
            top: 0,
            left: 0,
            blur: 3,
            color: '#000',
            opacity: 0.9
        }
    },
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: false,
        // width: 2,
        // colors: ['transparent']
    },
    xaxis: {
        categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
        labels: {
            style: {
                colors: '#666b77',
                fontSize: '14px',
                fontFamily: 'Roboto-Regular',
                fontWeight: 400,
                cssClass: 'apexcharts-yaxis-label',
            },
        },
    },
    yaxis: {
        labels: {
            style: {
                colors: '#666b77',
                fontSize: '14px',
                fontFamily: 'Roboto-Regular',
                fontWeight: 400,
                cssClass: 'apexcharts-yaxis-label',
            },
        },
    },
    colors: ['#0090FF'],
    fill: {
        opacity: 1,
        colors: ['#0080ff', '#d4d8de'],
        gradient: {
            shade: 'dark',
            type: "horizontal",
            shadeIntensity: 0.9,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 1,
            opacityTo: 1,
            stops: [0, 50, 100],
            colorStops: []
        },
    },
    legend: {
        show: false
    },
    states: {
        normal: {
            filter: {
                type: 'none',
                value: 0,
            }
        },
        hover: {
            filter: {
                type: 'darken',//hover colum data
                value: 0.9,
            }
        },
        active: {
            allowMultipleDataPointsSelection: false,
            filter: {
                type: 'darken',
                value: 0.35,
            }
        },
    },
    dropShadow: {
        enabled: true,
        top: 1,
        left: 1,
        blur: 1,
        color: '#000',
        opacity: 0.45
    }
};
export default optionsCol;