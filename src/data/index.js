import buy_guide from './buy_guide';
import chatbot from './chatbot'
import monitoring from './monitoring'
import projectList from './projectList'
import dataTableHistory from './dataTableHistory'
import dataSelectHistory from './dataSelectHistory'
import dataCardStatus from './dataCardStatus'
import optionsCol from './dataChartCol'
import optionsLine from './dataChartLine'
import user from './CIF/user'


export {
    buy_guide,
    chatbot,
    monitoring,
    projectList,
    dataTableHistory,
    dataSelectHistory,
    dataCardStatus,
    optionsCol,
    optionsLine,
    user,
}