const dataList = [
    { project_id: 1, project_name: 'SCB banking1', project_type: 'SCB banking1', project_role: 'SCB banking1', project_status: true },
    { project_id: 2, project_name: 'SCB banking2', project_type: 'SCB banking2', project_role: 'SCB banking2', project_status: false },
    { project_id: 3, project_name: 'SCB banking3', project_type: 'SCB banking3', project_role: 'SCB banking3', project_status: false },
    { project_id: 4, project_name: 'SCB banking4', project_type: 'SCB banking4', project_role: 'SCB banking4', project_status: true },
    { project_id: 5, project_name: 'SCB banking5', project_type: 'SCB banking5', project_role: 'SCB banking5', project_status: false },
]
export default dataList;