const dataCardStatus = [
    {
        title: "Đang hoạt động",
        number: "1235",
        icon: "las la-user-check",
        people: "Người",
        percent: "60% người tham gia",
    },
    {
        title: "Người tham gia",
        number: "2502",
        icon: "las la-user",
        people: "Người/Năm",
        percent: "+12% người tham gia",
    },
    {
        title: "",
        number: "825",
        icon: "las la-question-circle",
        people: "Tổng câu hỏi",
        percent: "",
    },
    {
        title: "",
        number: "568",
        icon: "las la-check-circle",
        people: "Câu trả lời",
        percent: "",
    },
    {
        title: "",
        number: "257",
        icon: "las la-exclamation-circle",
        people: "Câu không trả lời",
        percent: "",
    },

]
export default dataCardStatus;