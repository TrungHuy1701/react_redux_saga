const convertMili = (s) => {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;

    return hrs + ':' + mins + ':' + secs;
};  // convert date Milisecon to duration(hh/mm/s)

const dataTableHistory = [
    {
        id: 1,
        status: "xác nhận",
        visitor: "quang",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 2',
        Duration: convertMili(777600000),
        durationMili: 777600000,
        startDay: "2021-03-22T17:00:00.000Z",
        endDay: "2021-03-23T02:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 2,
        status: "Đang chờ",
        visitor: "Thuan",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 1',
        Duration: convertMili(677340000),
        durationMili: 677340000,
        startDay: "2021-03-20T17:00:00.000Z",
        endDay: "2021-03-23T02:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 3,
        status: "Đã hủy",
        visitor: "Hoa",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 3',
        Duration: convertMili(172834000),
        durationMili: 172834000,
        startDay: "2021-03-18T17:00:00.000Z",
        endDay: "2021-03-20T02:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 4,
        status: "Đang chờ",
        visitor: "Vinh",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 3',
        Duration: convertMili(86444000),
        durationMili: 86444000,
        startDay: "2021-03-22T17:00:00.000Z",
        endDay: "2021-03-23T18:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 5,
        status: "xác nhận",
        visitor: "dung",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 1',
        Duration: convertMili(86400000),
        durationMili: 86400000,
        startDay: "2021-03-22T16:00:00.000Z",
        endDay: "2021-03-23T20:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 6,
        status: "Đang chờ",
        visitor: "dung",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 2',
        Duration: convertMili(259200000),
        durationMili: 259200000,
        startDay: "2021-03-26T17:00:00.000Z",
        endDay: "2021-03-30T02:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 7,
        status: "xác nhận",
        visitor: "dung",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 1',
        Duration: convertMili(86400000),
        durationMili: 86400000,
        startDay: "2021-03-28T17:00:00.000Z",
        endDay: "2021-03-29T02:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 8,
        status: "Đã hủy",
        visitor: "dung",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 1',
        Duration: convertMili(85400000),
        durationMili: 85400000,
        startDay: "2021-03-29T17:00:00.000Z",
        endDay: "2021-03-31T02:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 9,
        status: "Đang chờ",
        visitor: "dung",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 2',
        Duration: convertMili(172560000),
        durationMili: 172560000,
        startDay: "2021-03-27T17:00:00.000Z",
        endDay: "2021-03-29T02:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 10,
        status: "Đã hủy",
        visitor: "dung",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 4',
        Duration: convertMili(173800000),
        durationMili: 173800000,
        startDay: "2021-03-28T17:00:00.000Z",
        endDay: "2021-03-30T02:00:00.000Z",
        //Export: <i className="fas fa-download table-icon"></i>
    },
    {
        id: 11,
        status: "Đã hủy",
        visitor: "dung",
        Agents: "_ _ _",
        message: 'Xin chao',
        project: 'SBC internet Banking 3',
        Duration: convertMili(162340000),
        durationMili: 162340000,
        //Export: <i className="fas fa-download table-icon"></i>
    },
]

export default dataTableHistory;