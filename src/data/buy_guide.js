const buy_guide = {
    vi: [
        {
            step: '2.1',
            title: 'Tiêu đề',
            field: "Click chọn các trường thông tin trên khung (1)",
            criteria: 'Để tìm kiếm dự án theo các tiêu chí sau:',
            categories: ' Vị trí (Thành phố/ Quận huyện) | Tiến độ | Phạm vi giá (tỷ đồng) | Diện tích(m2) | Loại hình sản phẩm',
            guideMouse: 'Sau đó click vào nút TÌM KIẾM',
            content: 'Để đi bắt đầu tìm kiếm dựa trên những tiêu chí đã chọn ở trên.',
            webGuide: ' Website sẽ hiển thị những dự án đúng với các tiêu chí bạn đã search.',
            guideImage: '../images/direction1.jpg'
        },
        {
            step: '2.2',
            title: 'Tiêu đề',
            field: 'Click chọn các trường thông tin trên khung (1)',
            criteria: 'Để tìm kiếm dự án theo các tiêu chí sau:',
            categories: ' Vị trí (Thành phố/ Quận huyện) | Tiến độ | Phạm vi giá (tỷ đồng) | Diện tích(m2) | Loại hình sản phẩm',
            guideMouse: 'Sau đó click vào nút TÌM KIẾM',
            content: 'Để đi bắt đầu tìm kiếm dựa trên những tiêu chí đã chọn ở trên.',
            webGuide: ' Website sẽ hiển thị những dự án đúng với các tiêu chí bạn đã search.',
            guideImage: '../images/direction1.jpg'
        },
        {
            step: '2.3',
            title: 'Tiêu đề',
            field: 'Click chọn các trường thông tin trên khung (1)',
            criteria: 'Để tìm kiếm dự án theo các tiêu chí sau:',
            categories: ' Vị trí (Thành phố/ Quận huyện) | Tiến độ | Phạm vi giá (tỷ đồng) | Diện tích(m2) | Loại hình sản phẩm',
            guideMouse: 'Sau đó click vào nút TÌM KIẾM',
            content: 'Để đi bắt đầu tìm kiếm dựa trên những tiêu chí đã chọn ở trên.',
            webGuide: ' Website sẽ hiển thị những dự án đúng với các tiêu chí bạn đã search.',
            guideImage: '../images/direction1.jpg'
        }
    ],
    en: [
        {
            step: '2.1',
            title: 'Title',
            field: "Click to select the information fields in the box (1)",
            criteria: 'To search for projects according to the following criteria:',
            categories: 'Location (City / District) | Progress | Price range (billion VND) | Area (m2) | Product type',
            guideMouse: 'Then click the SEARCH button',
            content: 'To go start searching based on the criteria selected above.',
            webGuide: 'Website will display projects that match the criteria you searched.',
            guideImage: '../images/direction1.jpg'
        },
        {
            step: '2.2',
            title: 'Title',
            field: 'Click to select the information fields in the box (1)',
            criteria: 'To search for projects according to the following criteria:',
            categories: 'Location (City / District) | Progress | Price range (billion VND) | Area (m2) | Product type',
            guideMouse: 'Then click the SEARCH button',
            content: 'To go start searching based on the criteria selected above.',
            webGuide: 'Website will display projects that match the criteria you searched.',
            guideImage: '../images/direction1.jpg'
        },
        {
            step: '2.3',
            title: 'Title',
            field: 'Click to select the information fields in the box (1)',
            criteria: 'To search for projects according to the following criteria:',
            categories: 'Location (City / District) | Progress | Price range (billion VND) | Area (m2) | Product type',
            guideMouse: 'Then click the SEARCH button',
            content: 'To go start searching based on the criteria selected above.',
            webGuide: 'Website will display projects that match the criteria you searched.',
            guideImage: '../images/direction1.jpg'
        }
    ]

}



export default buy_guide