import React, { lazy } from 'react';


const PageError404 = lazy(() => import('./templates/ErrorPage/Error404'));
const PageCIF = lazy(() => import('./templates/CIF'));

const MAIN = [
    {
        path: "/404",
        component: PageError404,
    },
    {
        path: "/",
        component: PageCIF,
    },

]

const getPathList = (DATA) => {
    var list = []
    for (var i = 0; i < DATA.length; i++) {
        if (DATA[i]) {
            list.push(DATA[i].path)
        }
    }
    // console.log({ list });
    return list
}

export { MAIN, getPathList }
