import React from 'react';
const Hearder = () => {
    return (
        <div className="cif-card cif-card__padding">
            <div className="cif--container">
                <div className="title__card ">
                    <span className="text">
                        Tìm kiếm</span>
                    <div className="title__action-button">
                        <i className="fas fa-plus-circle" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-4 col-md-4 col-sm-6 col-sx-12">
                        <div className="form-group">
                            <label class="label">
                                Search</label>
                            <input className="form-control" placeholder="Nhập" />
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-6 col-sx-12">
                        <div className="form-group">
                            <label class="label">
                                Họ và tên</label>
                            <input className="form-control" placeholder="Nhập" />
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-6 col-sx-12">
                        <div className="form-group">
                            <label class="label">
                                Loại khách hàng</label>
                            <select defaultValue="0" className="form-control" placeholder="Nhập" >
                                <option value="0">
                                    Tất cả</option>
                                <option value="1">
                                    Cá nhân</option>
                                <option value="2">
                                    Doanh nghiệp</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-6 col-sx-12">
                        <div className="form-group">
                            <label class="label">
                                Số điện thoại</label>
                            <input className="form-control" placeholder="Nhập" />
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-6 col-sx-12">
                        <div className="form-group">
                            <label class="label">
                                Ngày sinh</label>
                            <input className="form-control datepicker" defaultValue="01/01/2020" placeholder="Nhập" type="date" />
                        </div>
                    </div>
                </div>
                <div class="footer-action-btn">
                    <button type="" class="btn btn-white-outline mr-2">
                        <span>
                            Reset</span>
                    </button>
                    <button type="submit" class="btn btn-blue">
                        <span>
                            Search</span>
                    </button>
                </div>
            </div>

        </div>
    )
}

export default Hearder