import React from 'react';
const CIF = () => {
    return (
        <div className="cif--page">
            <div className="row">
                <div className="col-md-3 col-12 d-flex flex-column">
                    <div className="cif-card h-100 p-0">
                        <div className="cif--container">
                            <div class="title__card ">
                                <span class="text">
                                    Danh sách khách hàng </span>
                                <div class="title__action-button">
                                </div>
                            </div>
                            <div class="customer-card">
                                <div class="customer-card__filter">
                                    <div class="form-group item__filter">
                                        <label class="label">
                                        </label>
                                        <select defaultValue="0" className="form-control customs-select" placeholder="Nhập" >
                                            <option value="0">
                                                Tất cả</option>
                                            <option value="1">
                                                Nam</option>
                                            <option value="2">
                                                Nữ</option>
                                        </select>

                                    </div>
                                </div>
                                <div className="customer-card__list">
                                    {[...Array(20)].map((item, index) => {
                                        return (
                                            <div class="customer-card__item" key={index}>
                                                <div class="customer-card__avatar">
                                                    <div class="avatar-image member-vip">
                                                        <img class="avatar " src="../images/logo.png" alt="avatar" />
                                                    </div>
                                                    <button type="" class="ant-btn btn btn-red">
                                                        <span>
                                                            Detail</span>
                                                    </button>
                                                </div>
                                                <div class="customer-card__info">
                                                    <div class="name">
                                                        Nguyễn Văn A</div>
                                                    <div class="info-code">
                                                        <div>
                                                            <i class="icon las la-check-circle">
                                                            </i>
                                                            <span class="text">
                                                                Mã CIF : </span>
                                                        </div>
                                                        <span class="code">
                                                            CIF0123456</span>
                                                    </div>
                                                    <div class="info-code active">
                                                        <i class="icon las la-check-circle">
                                                        </i>
                                                        <span class="text">
                                                            SĐT : </span>
                                                        <span class="code">
                                                            0901234567</span>
                                                    </div>
                                                    <div class="info-action">
                                                        <button type="" class="ant-btn btn btn-red-outline">
                                                            <span>
                                                                Action 1</span>
                                                        </button>
                                                        <button type="" class="ant-btn btn btn-blue-outline">
                                                            <span>
                                                                Action 2</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })}

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div className="col-md-9 col-12 d-flex flex-column">
                    <div className="cif-card cif-card__padding h-100">
                        <div className="cif--container">
                            <div class="title__card ">
                                <span class="text">
                                    Thông tin khách hàng</span>
                                <div class="title__action-button">
                                </div>
                            </div>
                            <div class="cif--customer-info">
                                <div class="scb-formCustomerProfile">
                                    <div class="row">
                                        <div class="col-lg-3 d-flex flex-column">
                                            <div class="profile__avatar">
                                                <div class="picture">
                                                    <img src="../images/logo.png" class="img-camera" />
                                                    <div class="change_pic">
                                                        <input type="file" class="change_avatar" accept="image/jpeg" />
                                                        <i class="fas fa-camera icon-upload">
                                                        </i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 d-flex flex-column">
                                            <label class="label"> Họ và tên</label>
                                            <div class="form-group">
                                                <input class="form-control" placeholder="Nhâp tên" format="DD/MM/YYYY" type="text" />
                                            </div>

                                            <label class="label"></label>
                                            <div class="form-group">
                                                <input class="form-control" placeholder="Nhập tên khác" format="DD/MM/YYYY" type="text" />
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label class="label"> Số điện thoại</label>
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="Nhập phone" format="DD/MM/YYYY" type="text" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="label"> Email</label>
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="Nhập email" type="text" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label class="label">Giới tinh</label>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <div className="form-control radio__option">
                                                            <input className="radio" name="gender" type="radio" value="Nam" />
                                                            <label className="radio_label">Nam</label>
                                                        </div>
                                                        <div className="form-control radio__option">
                                                            <input className="radio" name="gender" type="radio" value="Nữ" />
                                                            <label className="radio_label">Nữ</label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div className="row mt-3">
                                        <div className="col-lg-6">
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Ngày sinh</label>
                                                    <div className="form-group">
                                                        <input className="form-control" placeholder="Nhập" type="date" value="2018-07-22" format="DD/MM/YYYY" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Nơi sinh</label>
                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <select defaultValue="0" className="form-control customs-select" placeholder="Nhập" >
                                                            <option value="1">Hồ Chí Minh</option>
                                                            <option value="2">Hà Nội</option>
                                                            <option value="3">Axìn-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        CMND/CCCD</label>

                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <input className="form-control" placeholder="Nhập" type="text" />
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Nơi cấp</label>
                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <select defaultValue="0" className="form-control customs-select" placeholder="Nhập" >
                                                            <option value="1">Hồ Chí Minh</option>
                                                            <option value="2">Hà Nội</option>
                                                            <option value="3">Axìn-</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Ngày hết hạn</label>
                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <input className="form-control datepicker" defaultValue="01/01/2020" placeholder="Nhập" type="date" />
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Quốc tichk</label>
                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <select defaultValue="null" className="form-control customs-select" placeholder="Nhập" >
                                                            <option value="1">Việt Nam</option>
                                                            <option value="2">Campuchia</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div className="col-lg-6">
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Mã CIF</label>
                                                    <div className="input_icf">
                                                        <div className="input_icf-form">
                                                            <div className="form-group">
                                                                <input className="form-control" placeholder="Nhập CIF" format="DD/MM/YYYY" type="text" defaultValue />
                                                            </div>
                                                        </div>
                                                        <i className="las la-play-circle" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Ngày mở CIF</label>

                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <input className="form-control datepicker" defaultValue="01/01/2020" placeholder="Nhập" type="date" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Xếp hạng TD</label>

                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <select defaultValue="0" className="form-control customs-select" placeholder="Nhập" >
                                                            <option value="1">A+</option>
                                                            <option value="2">A</option>
                                                            <option value="3">A-</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Nhóm KH</label>
                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <select defaultValue="null" className="form-control customs-select" placeholder="Nhập" >
                                                            <option value="1">Cá Nhân</option>
                                                            <option value="2">Doanh nghiệp</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Vip type</label>
                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <select defaultValue="null" className="form-control customs-select" placeholder="Nhập" >
                                                            <option value="1">Vip</option>
                                                            <option value="2">Supper Vip</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <label className="label">
                                                        Branch</label>
                                                    <div className="form-group">
                                                        <label className="label" />
                                                        <select defaultValue="null" className="form-control customs-select" placeholder="Nhập" >
                                                            <option value="1">Chi nhánh 1</option>
                                                            <option value="2">Chi nhánh 2</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div name-c="FormCustomerTab">
                                    <div className="tabs tabs-top">
                                        <div className="tabs-nav">
                                            <div className="tabs-nav-wrap">
                                                <div className="tabs-nav-list" style={{ transform: 'translate(0px, 0px)' }}>
                                                    <div className="tabs-tab tabs-tab-active">
                                                        <div role="tab" aria-selected="true" className="tabs-tab-btn" tabIndex={0} id="rc-tabs-0-tab-1" aria-controls="rc-tabs-0-panel-1">
                                                            TT chi tiết</div>
                                                    </div>
                                                    <div className="tabs-tab">
                                                        <div role="tab" aria-selected="false" className="tabs-tab-btn" tabIndex={0} id="rc-tabs-0-tab-2" aria-controls="rc-tabs-0-panel-2">
                                                            TT sản phẩm</div>
                                                    </div>
                                                    <div className="tabs-tab">
                                                        <div role="tab" aria-selected="false" className="tabs-tab-btn" tabIndex={0} id="rc-tabs-0-tab-3" aria-controls="rc-tabs-0-panel-3">
                                                            TT tài khoản</div>
                                                    </div>
                                                    <div className="tabs-tab">
                                                        <div role="tab" aria-selected="false" className="tabs-tab-btn" tabIndex={0} id="rc-tabs-0-tab-4" aria-controls="rc-tabs-0-panel-4">
                                                            Tài liệu liên quan</div>
                                                    </div>
                                                    <div className="tabs-ink-bar tabs-ink-bar-animated" style={{ left: 0, width: 62 }} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ant-tabs-content-holder">
                                            <div className="ant-tabs-content ant-tabs-content-top">
                                                <div role="tabpanel" tabIndex={0} aria-hidden="false" className="ant-tabs-tabpane ant-tabs-tabpane-active" id="rc-tabs-0-panel-1" aria-labelledby="rc-tabs-0-tab-1">
                                                    <div name-c="TabInfoDetail" className="scb-tab-infor-detail">
                                                        <div className="row">
                                                            <div className="col-lg-6">
                                                                <div className="form-group">
                                                                    <label className="label">
                                                                        Địa chỉ liên lạc</label>
                                                                    <input className="form-control" placeholder="Nhập" type="text" defaultValue />
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="label">
                                                                        Quận/Huyện</label>
                                                                    <select defaultValue="0" className="form-control customs-select" placeholder="Nhập" >
                                                                        <option value="1">Hồ Chí Minh</option>
                                                                        <option value="2">Hà Nội</option>
                                                                        <option value="3">Axìn-</option>
                                                                    </select>
                                                                </div>
                                                                <div className="form-group">
                                                                    <label className="label">
                                                                        Cơ quan công tác</label>
                                                                    <input className="form-control" placeholder="Nhập" format="DD/MM/YYYY" type="text" defaultValue />
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-6">
                                                                <div className="form-group">
                                                                    <label className="label">
                                                                        Tỉnh/TP</label>
                                                                    <select defaultValue="0" className="form-control customs-select" placeholder="Nhập" >
                                                                        <option value="1">Hồ Chí Minh</option>
                                                                        <option value="2">Hà Nội</option>
                                                                        <option value="3">Axìn-</option>
                                                                    </select>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="label">
                                                                        Phường/Xã</label>
                                                                    <select defaultValue="0" className="form-control customs-select" placeholder="Nhập" >
                                                                        <option value="1">Hồ Chí Minh</option>
                                                                        <option value="2">Hà Nội</option>
                                                                        <option value="3">Axìn-</option>
                                                                    </select>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="label">
                                                                        SDT cơ quan</label>
                                                                    <input className="form-control" placeholder="Nhập" format="DD/MM/YYYY" type="text" defaultValue />

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-lg-6">

                                                                <div className="ant-checkbox-group d-flex flex-column" id="work_id">
                                                                    <div className="label">
                                                                        Nghề nghiệp</div>
                                                                    <div className="row">
                                                                        <div className="col-lg-6 d-flex flex-column">
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="work" type="checkbox" value="1" />
                                                                                <label className="radio_label">Kế toán</label>
                                                                            </div>
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="work" type="checkbox" value="2" />
                                                                                <label className="radio_label">HS/SV</label>
                                                                            </div>
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="work" type="checkbox" value="3" />
                                                                                <label className="radio_label">Marketting/PR</label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-lg-6 d-flex flex-column">
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="work" type="checkbox" value="4" />
                                                                                <label className="radio_label">Kinh doan</label>
                                                                            </div>
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="work" type="checkbox" value="5" />
                                                                                <label className="radio_label">Giáo viên</label>
                                                                            </div>
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="work" type="checkbox" value="6" />
                                                                                <label className="radio_label">CNTT</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-lg-12 ">
                                                                            <div className="scb-checkbox-note ">

                                                                                <div className="form-group">
                                                                                    <div className="form-control radio__option">
                                                                                        <input className="radio" name="work" type="checkbox" value="7" />
                                                                                        <label className="radio_label">Khác</label>
                                                                                    </div>
                                                                                    <input name="work" type="text" />
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-6">

                                                                <div className="ant-radio-group ant-radio-group-outline d-flex flex-column" id="salary">
                                                                    <div className="label">
                                                                        Thu nhập</div>
                                                                    <div className="row">
                                                                        <div className="col-lg-6 d-flex flex-column">
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="salary" type="radio" value="1" />
                                                                                <label className="radio_label">Dưới 10 triệu</label>
                                                                            </div>
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="salary" type="radio" value="2" />
                                                                                <label className="radio_label">  30 triệu &lt; 70 triệu</label>
                                                                            </div>
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="salary" type="radio" value="3" />
                                                                                <label className="radio_label">   Trên 100 triệu</label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-lg-6 d-flex flex-column">
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="salary" type="radio" value="4" />
                                                                                <label className="radio_label">10  &lt; 30 triệu</label>
                                                                            </div>
                                                                            <div className="form-control radio__option">
                                                                                <input className="radio" name="salary" type="radio" value="5" />
                                                                                <label className="radio_label"> 70 &lt; 100 triệu</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-lg-12 ">
                                                                            <div className="scb-checkbox-note ">
                                                                                <div className="form-group">
                                                                                    <div className="form-control radio__option">
                                                                                        <input className="radio" name="salary" type="radio" value="6" />
                                                                                        <label className="radio_label">Khác</label>
                                                                                    </div>
                                                                                    <input name="salary" type="text" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-lg-3 label">
                                                                Tình trạng hôn nhân</div>
                                                            <div className="col-lg-9">

                                                                <div className="form-control radio__option">
                                                                    <input className="radio" name="salary" type="radio" value="6" />
                                                                    <label className="radio_label">Độc thân</label>
                                                                </div>
                                                                <div className="form-control radio__option">
                                                                    <input className="radio" name="salary" type="radio" value="6" />
                                                                    <label className="radio_label">Đã có gia đình</label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" tabIndex={-1} aria-hidden="true" className="ant-tabs-tabpane" id="rc-tabs-0-panel-2" aria-labelledby="rc-tabs-0-tab-2" style={{ display: 'none' }} />
                                                <div role="tabpanel" tabIndex={-1} aria-hidden="true" className="ant-tabs-tabpane" id="rc-tabs-0-panel-3" aria-labelledby="rc-tabs-0-tab-3" style={{ display: 'none' }} />
                                                <div role="tabpanel" tabIndex={-1} aria-hidden="true" className="ant-tabs-tabpane" id="rc-tabs-0-panel-4" aria-labelledby="rc-tabs-0-tab-4" style={{ display: 'none' }} />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="customer-sign">
                                    <div className="table-role">
                                        <div className="title__card ">
                                            <span className="text">
                                                NGƯỜI ĐẠI DIỆN/ GIÁM HỘ/ DÒNG CHỦ TÀI KHOẢN/ CHỦ SỢ HỮU</span>
                                            <div className="title__action-button">
                                                <i className="fas fa-minus-circle" />
                                            </div>
                                        </div>
                                        <div className="cus-table mb-3 table-fixed table-bottom">
                                            <table className="w-100">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" width="10%">
                                                            STT</th>
                                                        <th scope="col" width="40%">
                                                            Họ và tên</th>
                                                        <th scope="col" width="30%">
                                                            Vai trò</th>
                                                        <th scope="col" width="20%" />
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td width="10%">
                                                            1</td>
                                                        <td width="40%">
                                                            Nguyễn Văn A</td>
                                                        <td width="30%">
                                                            <div className="form-group">
                                                                <select defaultValue="1" className="form-control customs-select" placeholder="Nhập" >
                                                                    <option value="1">Người đại diện pháp luật 1</option>
                                                                    <option value="2">Người đại diện pháp luật 2</option>

                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td width="20%" className="text-right">
                                                            <i className="fas fa-trash-alt table-icon" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10%">
                                                            2</td>
                                                        <td width="40%">
                                                            Nguyễn Văn B</td>
                                                        <td width="30%">
                                                            <div className="form-group">
                                                                <select defaultValue="2" className="form-control customs-select" placeholder="Nhập" >
                                                                    <option value="1">Người đại diện pháp luật 1</option>
                                                                    <option value="2">Người đại diện pháp luật 2</option>

                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td width="20%" className="text-right">
                                                            <i className="fas fa-trash-alt table-icon" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10%">
                                                            3</td>
                                                        <td width="40%">
                                                            Nguyễn Văn C</td>
                                                        <td width="30%">
                                                            <div className="form-group">
                                                                <select defaultValue="1" className="form-control customs-select" placeholder="Nhập" >
                                                                    <option value="1">Người đại diện pháp luật 1</option>
                                                                    <option value="2">Người đại diện pháp luật 2</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td width="20%" className="text-right">
                                                            <i className="fas fa-trash-alt table-icon" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <label className="text-decoration">
                                            + Thêm</label>
                                    </div>
                                    <div className="table-relationship">
                                        <div className="title__card ">
                                            <span className="text">
                                                MỐI QUAN HỆ NHÂN THÂN</span>

                                            <div className="title__action-button">
                                                <i className="fas fa-minus-circle" />
                                            </div>
                                        </div>
                                        <div className="cus-table mb-3 table-fixed ">
                                            <table className="w-100">

                                                <thead>
                                                    <tr>
                                                        <th scope="col" width="10%">
                                                            STT</th>
                                                        <th scope="col" width="40%">
                                                            Họ và tên</th>
                                                        <th scope="col" width="30%">
                                                            Quan hệ</th>
                                                        <th scope="col" width="20%" />

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td width="10%">
                                                            1</td>
                                                        <td width="40%">
                                                            Nguyễn Văn A</td>
                                                        <td width="30%">
                                                            <div className="form-group">
                                                                <select defaultValue="1" className="form-control customs-select" placeholder="Nhập" >
                                                                    <option value="1">Chồng</option>
                                                                    <option value="2">Vợ</option>
                                                                </select>

                                                            </div>
                                                        </td>
                                                        <td width="20%" className="text-right">
                                                            <i className="fas fa-trash-alt table-icon" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <label className="text-decoration"> + Thêm</label>

                                    </div>
                                    <div className="signature">
                                        <div className="title__card ">

                                            <span className="text">
                                                CHỮ KÝ MẪU</span>
                                            <div className="title__action-button">
                                                <i className="fas fa-minus-circle" />
                                            </div>
                                        </div>
                                        <div className="row">

                                            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <span className="ant-upload-picture-card-wrapper">
                                                    <div className="ant-upload-list ant-upload-list-picture-card">
                                                        <div className="ant-upload ant-upload-select ant-upload-select-picture-card">
                                                            <span tabIndex={0} className="ant-upload" role="button">
                                                                <input type="file" accept style={{ display: 'none' }} />
                                                                <div className="list-upload">
                                                                    <i className="far fa-images list-upload__icon" />
                                                                    <span className="list-upload__text">
                                                                        Upload Image</span>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                        <button type className=" btn btn-blue">
                                            <span>
                                                Save File</span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {/* ------------ */}
        </div >
    )
}
export default CIF