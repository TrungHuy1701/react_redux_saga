import React, { Suspense, useState, useEffect } from 'react';
import { MAIN } from '../routes';
import { Error404 } from '../templates/ErrorPage';
import { Switch, Route, useLocation, Redirect } from "react-router-dom";
import { matchPath } from "react-router";


const MainPage = (props) => {

    const location = useLocation();
    const currentRoute = MAIN.find(
        route => {
            let params = matchPath(location.pathname, { path: route.path });
            if (params?.isExact) { return params }
        }
    )

    return (
        <div className="main_wapper">
            {/* <Header title={currentRoute?.title} /> */}
            <div className={`main_container`}>
                {/* <SideBar /> */}
                <div className="main_content">
                    <Suspense fallback={''}>
                        <Switch >
                            {MAIN.map((data, idx) =>
                            (
                                <Route exact key={idx} path={data.path}>
                                    <data.component />
                                    {/* { data.path == '/' && <Redirect to={PAGES_URL.dashboard.url} />} */}
                                </Route>
                            ))}
                            <Route component={Error404} />
                        </Switch>
                    </Suspense>
                </div>
            </div>
        </div>
    )
}

export default MainPage;