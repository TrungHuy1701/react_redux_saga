//libs
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import { getPathList, MAIN } from '../routes';
import MainPage from './MainPage';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        <MainPage />
    )} />
)

const App = () => {
    return (
        <Router>
            <Switch >
                <Route exact path={getPathList(MAIN)} >
                    <Route render={props => <PrivateRoute {...props} />} />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
